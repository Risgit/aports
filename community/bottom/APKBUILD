# Contributor: guddaff <guddaff@protonmail.com>
# Maintainer: guddaff <guddaff@protonmail.com>
pkgname=bottom
pkgver=0.6.3
pkgrel=1
pkgdesc="A graphical process/system monitor with a customizable interface"
url="https://github.com/ClementTsang/bottom"
arch="x86_64 armv7 armhf aarch64 x86 ppc64le"  # limited by rust/cargo
license="MIT"
makedepends="cargo"
subpackages="
	$pkgname-fish-completion
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-doc
	"
source="https://github.com/ClementTsang/bottom/archive/$pkgver/$pkgname-$pkgver.tar.gz
	minimize-size.patch
	"

build() {
	cargo build --release --locked
}

check() {
	CARGO_HUSKY_DONT_INSTALL_HOOKS=true cargo test --locked
}

package() {
	cargo install --locked --path . --root=$pkgdir/usr
	rm "$pkgdir"/usr/.crates*

	install -Dm644 sample_configs/default_config.toml -t "$pkgdir"/usr/share/doc/$pkgname/

	cd target/release/build/bottom-*/out
	install -Dm644 _btm "$pkgdir"/usr/share/zsh/site-functions/_btm
	install -Dm644 btm.bash "$pkgdir"/usr/share/bash-completion/completions/btm
	install -Dm644 btm.fish "$pkgdir"/usr/share/fish/completions//btm.fish
}

sha512sums="
1c27a3b2cc264879b669517ab6e67748b7ce6db5d07850aea829ae4c6be86bfce1ca718d6af0cd556d5d9196643b4df98186515212571f72f043ac91b2211780  bottom-0.6.3.tar.gz
67bd8e4acc4326dfa1a313ce72b2b20d2c11a7894f9fddfafa62151763679578e94d99a51aa3667d64dbc85368453a1e91ae95c78df5809d34c6d49db59c01ff  minimize-size.patch
"
