# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=black
pkgver=21.8_beta0
_pkgver=${pkgver/_beta/b}
pkgrel=0
pkgdesc="The uncompromising Python code formatter"
url="https://github.com/psf/black"
arch="noarch !mips !mips64" # tests fail on mips
license="MIT"
depends="python3 py3-click py3-attrs py3-tomli py3-platformdirs py3-typed-ast py3-regex
	py3-pathspec py3-typing-extensions py3-mypy-extensions"
makedepends="py3-setuptools py3-setuptools_scm"
checkdepends="py3-pytest py3-parameterized py3-aiohttp py3-aiohttp-cors"
source="https://files.pythonhosted.org/packages/source/b/black/black-$_pkgver.tar.gz
	remove-setuptools_scm.patch
	"
builddir="$srcdir/$pkgname-$_pkgver"

build() {
	python3 setup.py build
}

check() {
	# temporary installation for testing
	python3 setup.py install --root="$PWD"/test_install --skip-build

	PATH="$PWD/test_install/usr/bin:$PATH" \
		PYTHONPATH="$(echo $PWD/test_install/usr/lib/python3*/site-packages)" \
		pytest -m "not without_python2"
}

package() {
	python3 setup.py install --root="$pkgdir" --skip-build
}

sha512sums="
1a245baabdda601f3de65bbf6abd73874bae85c83f072407bffb83ecd7ff5ef73c1a4d2655685bc9c9613eae7c7bef0eb5b5535510332059b488c13b809194f2  black-21.8b0.tar.gz
c331fc1e388e069d06a756e06a36a05b259ccb45074d69aaf4250360b7d0f72a7b5c3f19903c4452466f9d96b1cc41aaa640b2399231126f31b29309206ca644  remove-setuptools_scm.patch
"
