# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Maintainer:
pkgname=py3-fonttools
_pkgname=fonttools
pkgver=4.26.2
pkgrel=0
pkgdesc="Converts OpenType and TrueType fonts to and from XML"
options="!check" # Require unpackaged 'brotli'
url="https://github.com/fonttools/fonttools"
arch="all"
license="MIT AND OFL-1.1"
depends="py3-lxml py3-fs cython python3-dev"
makedepends="py3-setuptools"
checkdepends="py3-pytest"
subpackages="$pkgname-doc"
source="$_pkgname-$pkgver.tar.gz::https://github.com/fonttools/fonttools/archive/$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-fonttools" # Backwards compatibility
provides="py-fonttools=$pkgver-r$pkgrel" # Backwards compatibility

prepare() {
	default_prepare
	# remove interpreter line
	sed -i '1d' Lib/fontTools/mtiLib/__init__.py
}

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD/build/lib" py.test-3
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

doc() {
	replaces="py-$_pkgname-doc" # Backwards compatibility
	provides="py-$_pkgname-doc=$pkgver-r$pkgrel" # Backwards compatibility
	default_doc
}

sha512sums="
2caa52a48574e6bdf418fe57fb4ca69686eeae65cb1f44c0cb16c64400ea666530dfcca9e8a619859cf553435f93b38f63e422c315206ecec467eed6339a5987  fonttools-4.26.2.tar.gz
"
