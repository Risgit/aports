# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libcap
pkgver=2.56
pkgrel=0
pkgdesc="POSIX 1003.1e capabilities"
arch="all"
license="BSD-3-Clause OR GPL-2.0-only"
url="https://sites.google.com/site/fullycapable/"
depends_dev="linux-headers"
makedepends_build="linux-headers perl bash"
makedepends_host="$depends_dev"
makedepends="$makedepends_build $makedepends_host"
subpackages="$pkgname-doc $pkgname-static $pkgname-dev"
source="https://kernel.org/pub/linux/libs/security/linux-privs/libcap2/libcap-$pkgver.tar.xz"

# faulty test of libcap.so itself (?!) on mips
[ "$CARCH" = "mips64" ] && options="!check"

build() {
	make BUILD_CC=gcc CC="${CC:-gcc}" lib=lib prefix=/usr GOLANG=no \
		DESTDIR="$pkgdir"
}

check() {
	make -j1 test
}

package() {
	make lib=lib prefix=/usr RAISE_SETFCAP=no GOLANG=no DESTDIR="$pkgdir" \
		install
	# Fix perms
	chmod -v 0755 "$pkgdir"/usr/lib/libcap.so.$pkgver
}

static() {
	depends=""
	pkgdesc="$pkgdesc (static library)"

	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib
}

sha512sums="
8a5c6c99458b0996c9599e00648e99e53b222ddf429535c475099c1af02796331b864fbfae767c16cfb7f2899cf0beaf74095e26a52988361ea1400f012f30c8  libcap-2.56.tar.xz
"
